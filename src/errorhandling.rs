// Error handling
use failure::{Context, Fail}; // format_err?

#[derive(Fail, Debug)]
#[fail(display = "Internal failure: invariant broken")]
pub struct InvariantError;
pub type Res<T> = Result<T, Context<String>>;

// TODO instead of context should I use Error+InvariantError(String )?