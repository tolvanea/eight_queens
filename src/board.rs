use crate::my_prelude::*;
use crate::errorhandling::{Res, InvariantError};

#[derive(Clone,Copy,Debug)]
pub enum TableValue {
    Occupied {idx: usize},   /// This square have queen, and array index for fast searching
    Empty,
}

impl TableValue{
    pub fn unwrap(self) -> usize{
        match self {
            TableValue::Occupied{idx} => idx,
            TableValue::Empty => panic!("Unwrap failed"),
        }
    }
}

#[derive(Clone,Debug)]
pub struct Board {
    pub table: Array2<TableValue>,
    pub ids: Array1<(usize, usize)>,
    pub queens: usize,
    pub side_length: usize,
    pub rng: StdRng,
}


//private impls
impl Board {
    fn general_invariants(&self) -> Res<()> {
        // queen_ amount
        let total_sum = Board::bool_sum(self.table.iter());
        if total_sum != self.queens {
            return Err(InvariantError
                .context("Board does not have correct number of queens.".to_string())
            );
        }

        // shape
        if self.table.shape()[0] != self.table.shape()[1] {
            return Err(InvariantError.context("Board is not square.".to_string()));
        }
        return Ok(());
    }

    fn invariant_ids_and_table_match(&self) -> Res<()> {
        for (i, &(x, y)) in self.ids.iter().enumerate() {
            match self.table[[x,y]] {
                TableValue::Occupied{idx} => {
                    if idx != i {
                        return Err(InvariantError.context("Table idx does not match ids.".to_string()));
                    }
                }
                TableValue::Empty => {
                    return Err(InvariantError.context("Ids does not match table.".to_string()));
                }
            }
        }
        return Ok(());
    }

    // Check number of queens in part of board
    fn bool_sum<'a, I>(table: I) -> usize
        where I: Iterator<Item=&'a TableValue> {
        use TableValue::*;
        return table.fold(0, |acc, a| acc + match *a {Occupied {idx:_} => 1, Empty => 0} );
    }
}
// Public impls
impl Board {
    pub fn create_random(side_length: usize, queens: usize) -> Board{
        let mut table = Array2::from_elem((side_length, side_length), TableValue::Empty);
        let seed = thread_rng().gen::<u64>();
        let mut rng: StdRng = SeedableRng::seed_from_u64(seed);
        let mut ids = Vec::<(usize, usize)>::new(); // (x, y)
        while ids.len() < queens {
            let x = rng.gen_range(0, side_length);
            let y = rng.gen_range(0, side_length);
            if None == ids.iter().position(|&v| v == (x, y)) {
                ids.push((x, y));
            }
        }
        for (i, &(x, y)) in ids.iter().enumerate() {
            table[[x,y]] = TableValue::Occupied {idx: i}
        }
        let board = Board{
            table: table,
            ids: Array1::from_vec(ids),
            queens: queens,
            side_length: side_length,
            rng: rng};
        if let Err(_) = board.invariant() {
            assert!(false, "Invarant broken");
        }
        return board;
    }

    fn with_seed(&mut self, seed: u64){
        let rng: StdRng = SeedableRng::seed_from_u64(seed);
        self.rng = rng;
    }

    /// Check validity of board: How many queens can eat each other? Ideally 0.
    pub fn overlaps(&self) -> i32 {
        // Generate arrays of diagonals
        // God darnit diagonal checking is laborous. This code is adapted from previous project
        // (tic tac toe)
        mod diagonal_checking {
            use ndarray as nd;

            // If you re-use this code, this should be the only thing you need use, see example code
            // below this module
            pub struct DiagonalGenerator<'a, BoardValue> {
                pub table: &'a nd::Array2<BoardValue>,
                pub count: usize,
            }

            #[derive(Clone, Copy, Debug, PartialEq)]
            enum Pos { Up, Right, Down, Left, }

            impl<'a, BoardValue> DiagonalGenerator<'a, BoardValue> {
                fn get_index_on_outer_edge(counter: usize, side_len: usize)
                                           -> (usize, usize, Pos, usize) {
                    fn outer_edge_1d_projection_idx(counter: usize, side_len: usize)
                                                    -> (usize, Pos, usize) {
                        if counter < side_len - 1 {
                            return (counter,
                                    Pos::Up,
                                    counter + 1);
                        } else if (counter >= side_len - 1) && (counter < 2 * side_len - 2) {
                            return (side_len - 1,
                                    Pos::Right,
                                    2 * side_len - 1 - counter);
                        } else if (counter >= 2 * side_len - 2) && (counter < 3 * side_len - 3) {
                            return (side_len - 1 - counter % (2 * side_len - 2),
                                    Pos::Down,
                                    counter - (2 * side_len - 2) + 1);
                        } else if (counter >= 3 * side_len - 3) && (counter < 4 * side_len - 4) {
                            return (0,
                                    Pos::Left,
                                    4 * side_len - 3 - counter);
                        } else {
                            assert!(false);
                            return (0, Pos::Down, 0);  // Pleasing rust compiler...
                        }
                    }
                    assert!(counter < 4 * side_len - 4);
                    let (x_idx, pos, diag_len) = outer_edge_1d_projection_idx(
                        counter, side_len);
                    let (y_idx, _, _) = outer_edge_1d_projection_idx(
                        (counter + (3 * side_len - 3)) % (4 * side_len - 4), side_len);
                    return (x_idx, y_idx, pos, diag_len);
                }
            }

            impl<'a, BoardValue: Copy> Iterator for DiagonalGenerator<'a, BoardValue> {
                type Item = nd::Array1<BoardValue>;
                fn next(&mut self) -> Option<Self::Item> {
                    let side_len = self.table.shape()[0];
                    if self.count >= 4 * side_len - 4 {
                        return None;
                    }

                    let (x_idx, y_idx, pos, diag_len) = Self::get_index_on_outer_edge(
                        self.count, side_len);
                    let arr;

                    if (pos == Pos::Up) || (pos == Pos::Right) {
                        arr = nd::Array1::from_shape_fn(diag_len, |i| self.table[[y_idx + i, x_idx - i]])
                    } else {
                        let (x_idx, y_idx, _pos, _diag_len) = Self::get_index_on_outer_edge(
                            self.count - (side_len - 1), side_len);
                        arr = nd::Array1::from_shape_fn(diag_len, |i| self.table[[y_idx - i, x_idx - i]])
                    }

                    self.count += 1;
                    return Some(arr);
                }
            }
        }

        let mut sum = 0;

        // Rows
        for row in self.table.outer_iter() {
            let s = Board::bool_sum(row.iter());
            sum += if s > 0 {s-1} else {s};
        }

        // Columns
        for col in self.table.axis_iter(Axis(1)) {
            let s = Board::bool_sum(col.iter());
            sum += if s > 0 {s-1} else {s};
        }

        // Diagonals
        let do_i_need_tmp_value_for_this =
            diagonal_checking::DiagonalGenerator { table: &self.table, count: 0 };
        for diag in do_i_need_tmp_value_for_this {
            let s = Board::bool_sum(diag.iter());
            sum += if s > 0 {s-1} else {s};
        }

        return sum as i32;
    }

    pub fn draw(&self) {
        for row in self.table.outer_iter() {
            for square in row.iter() {
                match *square {
                    TableValue::Occupied{idx:_} => print!("Q "),
                    TableValue::Empty => print!("* "),
                }
            }
            println!();
        }
    }

    pub fn invariant(&self) -> Res<()> {
        self.general_invariants()?;
        self.invariant_ids_and_table_match()?;
        return Ok(());
    }
}