use crate::my_prelude::*;
use crate::errorhandling::{Res, InvariantError};
use crate::board::{Board, TableValue};


pub fn solve(mut board: Board, temp: f64, blocks: usize) -> (Board, usize) {
    for b in 0..blocks {
        let fitness_old_state = board.overlaps();
        let new_board = random_move(board.clone());
        let fitness_new_state = new_board.overlaps();
        if fitness_new_state == 0 {
            return (new_board, b);
        }
        let diff = (fitness_new_state - fitness_old_state) as f64;
        let rnd = board.rng.gen_range(0.0, 1.0);
        if (-diff / temp).exp() > rnd {
            board = new_board;
        }
    }
    return (board, blocks);
}

fn random_move(mut board: Board) -> Board {
    let rng = &mut board.rng;
    let id = rng.gen_range(0,board.queens);
    let old_cell = board.ids[id];
    let mut new_cell = None;
    while new_cell == None {
        let x = rng.gen_range(0, board.side_length);
        let y = rng.gen_range(0, board.side_length);
        new_cell = match board.table[[x,y]] {
            TableValue::Occupied{idx: _} => None,
            TableValue::Empty => Some((x,y)),
        }
    }
    let new_cell = new_cell.unwrap();
    board.table[old_cell] = TableValue::Empty;
    board.table[new_cell] = TableValue::Occupied {idx: id};
    board.ids[id] = new_cell;
    match board.invariant() {
        Ok(()) => (),
        Err(e) => panic!(e.get_context().clone())
    };
    return board;
}