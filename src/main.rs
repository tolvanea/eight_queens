mod board;
mod metropolis;
mod errorhandling;
mod my_prelude;

use my_prelude::*;
use errorhandling::{Res};
use board::{Board};


// Bookkeeping
// For N/N problems 1e-2 1e-1 temperatures are the best
// 1e-16, 1e-8, 1e-4, 0.01, 0.1, 1.0

// 8*8 1000 itr
//Temp: 1e-3, iters: 6751.926(211.8682)
//Temp: 3e-2, iters: 6695.211(214.41592)
//Temp: 1e-2, iters: 6500.376(208.91476)
//Temp: 3e-1, iters: 13054.258(398.12268)
//Temp: 1e-1, iters: 6910.631(237.04788)

// 7*7 1000 itr
//Temp: 1e-3, iters: 3092.984(330.0652)
//Temp: 3e-2, iters: 2351.942(206.46275)
//Temp: 1e-2, iters: 3407.952(381.9776)
//Temp: 3e-1, iters: 2270.322(65.466354)
//Temp: 1e-1, iters: 2458.719(229.02956)

// 6*6, 1000 itr
//Temp: 1e-3, iters: 7336.853(563.86383)
//Temp: 3e-2, iters: 7208.932(564.44934)
//Temp: 1e-2, iters: 7358.142(563.7648)
//Temp: 3e-1, iters: 3990.911(126.471725)
//Temp: 1e-1, iters: 7420.626(554.57477)

// 5*5, 1000 itr
//Temp: 1e-3, iters: 299.184(8.515928)
//Temp: 3e-2, iters: 315.067(8.659854)
//Temp: 1e-2, iters: 305.186(8.514224)
//Temp: 3e-1, iters: 373.479(10.439445)
//Temp: 1e-1, iters: 416.525(100.07688)

fn run_one() {
    let board = Board::create_random(8, 8);
    board.draw();
    println!("");
    let (board_sol, iters) = metropolis::solve(board.clone(), 3e-1, 100000);
    board_sol.draw();
    println!("Iters: {}", iters)
}


fn benchmark() {
    for &temp in [1e-3, 3e-2, 1e-2, 3e-1, 1e-1].iter() { //
        let num_samples = 1000;
        let problem = |_| {
            let board = Board::create_random(8, 8);
            let (_board_sol, iters) = metropolis::solve(board.clone(), temp, 100000);
            iters as f32
        };
        let samples: Array1<f32> = (0..num_samples).map(problem).collect();
        let mean = samples.mean_axis(Axis(0));
        let std = samples.std_axis(Axis(0), 0.0) / (samples.len() as f32).sqrt();
        println!("Temp: {:e}, iters: {}({})", temp, mean, std);
    }
}


fn main() -> Res<()> {
    //benchmark();
    run_one();
    return Ok(());
}
