# Eight queens

Solve eight queens -problem by using Hastings-algorithm.

Eight queens is a problem in which one places 8 queens on chess board so that none of them can eat each other.

Hastings algorithm is a simple but effective discrete optimization method based on random Markov chain.

If this program is run with
```
cargo run --release
```
it finds a solution in a fraction of second:
```
* * * * Q * * *
* * * * * * * Q
* * * Q * * * *
Q * * * * * * *
* * Q * * * * *
* * * * * Q * *
* Q * * * * * *
* * * * * * Q *
Iters: 313
```

### Potential improvements with simulated annealing
Simulated annealing is a method in which markov chain steps in state space are initially large and aggressive, but as it gets closer to solution, the smaller steps are taken. Simulated annealing can avoid getting stuck in local minima too early. So far this code first finds optimal randomness for the steps, and then locks that as a constant during algorithm.
